from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from selenium import webdriver

# Create your tests here.
class JenggotHajiUnitTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(JenggotHajiUnitTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(JenggotHajiUnitTest, self).tearDown()

    def test_get_homepage(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000')

    def test_root_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_home_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
